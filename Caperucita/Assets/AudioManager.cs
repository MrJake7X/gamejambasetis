﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    //ENTORNo
    public AudioSource animalesPoseidos;
    public AudioSource arbolesOjos;
    //Cordura
    public AudioSource pCordura1;
    public AudioSource pCordura2;
    public AudioSource pCordura3;
    public AudioSource ambientCorduraBaja;
    public AudioSource ambientCorduraAlta;
    //Player
    public AudioSource pasos;

    //Paranoias
    public AudioSource cardActivate;
    public AudioSource despacio;
    public AudioSource alreves;
    public AudioSource correr;
    public AudioSource animales;
    public AudioSource flores;
    public AudioSource oscuridad;
    public AudioSource trampas;
    public AudioSource arboles;


}
