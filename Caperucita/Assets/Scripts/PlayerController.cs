﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private GameManager gameManager;
    private AudioManager audio;
    private Rigidbody rb;

    private Animator anim;

    // Movement speed
    [Header("Movement Properties")]
    public float speed;
    public float maxSpeed;
    private float iniMaxSpeed;
    private float axisX;
    private float axisY;
    private Vector2 axis;
    private Vector3 movement;
    private Vector3 lastMovement;

    // Model to rotate
    public Transform mesh;

    [Header("Interact Properties")]
    public float distance;
    public LayerMask mask;

    [Header("Slow Properties")]
    public float slowDuration;
    private float iniSlowDuration;
    private bool slowed;
    private float slowCounter;

    [Header("Stun Properties")]
    public float stunDuration;
    private float iniStunDuration;
    public bool stunned;
    private float stunCounter;

    [Header("Confusion Properties")]
    public float confusionDuration;
    private float iniConfusionDuration;
    private bool confused;
    private float confusionCounter;

    [Header("Fast Properties")]
    public float fastDuration;
    private float iniFastDuration;
    public float fastSpeed;
    private bool onFast;
    private float fastCounter;

    private bool take;
    private float counter;
    
    void Start()
    {
        rb = GetComponent<Rigidbody>();

        anim = GetComponentInChildren<Animator>();

        gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();

        audio = GameObject.Find("Audios").GetComponent<AudioManager>();

        iniMaxSpeed = maxSpeed;
        iniConfusionDuration = confusionDuration;
        iniFastDuration = fastDuration;
        iniSlowDuration = slowDuration;
        iniStunDuration = stunDuration;
    }

    void Update()
    {
        #region Movement

        if (take)
        {
            axis.y = 0;
            axis.x = 0;
            counter += Time.deltaTime;
            if(counter >= 7)
            {
                counter = 0;
                take = false;
            }
        }
        else
        {
            anim.SetBool("take", take);
            if (confused)
            {
                axis.y = Input.GetAxis("HorizontalMove");
                axis.x = Input.GetAxis("VerticalMove");

                confusionCounter += Time.deltaTime;
                if (confusionCounter >= confusionDuration)
                {
                    Confusion();
                }
            }
            else
            {
                axis.x = Input.GetAxis("HorizontalMove");
                axis.y = Input.GetAxis("VerticalMove");
            }
        }
        

        movement = new Vector3(axis.x, 0, axis.y).normalized;

        if(movement != Vector3.zero)
        {
            lastMovement = movement;
            audio.pasos.Play();
            anim.SetBool("walk", true);
        }
        else
        {
            audio.pasos.Stop();
            anim.SetBool("walk", false);
        }

        if (onFast)
        {
            rb.drag = 0;
            fastCounter += Time.deltaTime;
            if (fastCounter >= fastDuration)
            {
                Fast();
            }
        }
        else
        {
            rb.drag = 1;
        }

        if (slowed)
        {
            slowCounter += Time.deltaTime;
            if (slowCounter >= slowDuration)
            {
                maxSpeed = iniMaxSpeed;
                slowCounter = 0;
                slowed = false;
            }
        }

        if(stunned)
        {
            rb.velocity = Vector3.zero;
            if (stunCounter < stunDuration)
            {
                stunCounter += Time.deltaTime;
            }
            else
            {
                stunCounter = 0;
                stunned = false;
            }
        }
        else
        {
            if (movement != Vector3.zero)
            {
                mesh.rotation = Quaternion.LookRotation(movement);
            }
        }
        #endregion

    }
    private void FixedUpdate()
    {
        if(!stunned && !onFast)
        {
            rb.AddForce(movement * speed);
        } else if(onFast)
        {
            rb.AddForce(lastMovement * speed);
        }

        if(rb.velocity.magnitude > maxSpeed)
        {
            float brakeSpeed = speed - maxSpeed;

            Vector3 brakeVelocity = rb.velocity.normalized * brakeSpeed;

            rb.AddForce(-brakeVelocity);
        }

        RaycastHit hit;

        if (Physics.Raycast(transform.position, mesh.TransformDirection(Vector3.forward), out hit, distance, mask))
        {
            if (Input.GetKeyDown(KeyCode.F))
            {
                take = true;
                anim.SetBool("take", take);

                if (hit.transform.CompareTag("Conejo"))
                {
                    hit.transform.gameObject.GetComponent<Conejito>().Use();
                }
                else if(hit.transform.CompareTag("Flor"))
                {
                    hit.transform.gameObject.GetComponent<Flor>().Use();
                }
            }
        }
    }

    public void Stunn()
    {
        if(!stunned)
        {
            if (gameManager.onExtra)
            {
                stunDuration += gameManager.extraTime;
            }
            else stunDuration = iniStunDuration;

            stunned = true;
        }
    }

    public void Slow()
    {
        if (!slowed)
        {
            if (gameManager.onExtra)
            {
                slowDuration += gameManager.extraTime;
            }
            else slowDuration = iniSlowDuration;

            maxSpeed = maxSpeed / 2;
            slowed = true;
        }
    }

    public void Confusion()
    {
        if (!confused)
        {
            Debug.Log("Confusion");
            if (gameManager.onExtra)
            {
                confusionDuration += gameManager.extraTime;
            }
            else confusionDuration = iniConfusionDuration;

            confused = true;
        }
        else
        {
            Debug.Log("Adios Confusion");
            confusionCounter = 0;
            confused = false;
        }
    }

    public void Fast()
    {
        if(!onFast)
        {
            maxSpeed = fastSpeed;
            if (gameManager.onExtra)
            {
                fastDuration += gameManager.extraTime;
            }
            else fastDuration = iniFastDuration;

            onFast = true;
        }
        else
        {
            fastCounter = 0;
            onFast = false;
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;

        Gizmos.DrawRay(transform.position, mesh.TransformDirection(Vector3.forward) * distance);
    }
}
