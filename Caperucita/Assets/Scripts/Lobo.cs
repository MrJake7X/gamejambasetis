﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class Lobo : MonoBehaviour
{
    Transform player;
    public Insanity cordura;
    public float minimoPaExistir;
    public float distanciaAtaque;
    public float distanciaAparicion;
    public int ME = 0;

    //NAV MESH
    NavMeshAgent agent;

    void Start()
    {
        ME = 4;
        player = GameObject.FindWithTag("Player").transform;
        agent = GetComponent<NavMeshAgent>();
        agent.stoppingDistance = distanciaAtaque;
        GetComponent<MeshRenderer>().enabled = false;
        PosBehindPlayer();
    }
    void PosBehindPlayer()
    {
        transform.position = new Vector3(
            player.position.x, transform.position.y, player.position.z
            ) - player.transform.forward * distanciaAparicion;
    }
    void Update()
    {
        switch (ME)
        {
            case 0://apareciendo
                agent.Stop();
                GetComponent<MeshRenderer>().enabled = true;
                if (true)//fin de la animacion
                    ME++;
                break;
            case 1:// persiguiendo
                agent.SetDestination(player.position);
                agent.Resume();
                if (cordura.GetCurrentInanity() < minimoPaExistir) { ME = 3; } //control de si existo
                //a rango de ataque
                if (distanciaAtaque > Vector2.Distance(new Vector2(transform.position.x, transform.position.z), new Vector2(player.position.x, player.position.z)))
                { ME = 2; }
                break; 
            case 2:
                agent.Stop();
                if (cordura.GetCurrentInanity() < minimoPaExistir) { ME = 3; } //control de si existo
                if (false) {/*animacio idel?*/ /*WIN PLAYER 2*/ } //te toca
                else { ME = 1; } //no te toca y termina la animacion
                break; // te mata
            case 3:
                    agent.Stop();
                    if (true) { ME = 4; } break; //desaparece
            case 4:
                agent.Stop();
                GetComponent<MeshRenderer>().enabled = false;
                PosBehindPlayer();
                if (cordura.GetCurrentInanity() > minimoPaExistir) { ME = 0; }
                break; //No esta (invisible y qieto)
        }
    }
}
