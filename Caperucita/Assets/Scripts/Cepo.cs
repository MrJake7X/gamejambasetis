﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cepo : MonoBehaviour {

    private GameManager gameManager;

    public GameObject meshNormal;
    public GameObject meshDisfrazada;

    private Animator anim;

    private bool hided;
    private float timeCounter;
    public float hideDuration;
    private float iniHideDuration;

    private void Start()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();

        anim = GetComponentInChildren<Animator>();

        iniHideDuration = hideDuration;

        meshNormal.SetActive(true);
        meshDisfrazada.SetActive(false);
    }

    void Update()
    {
        if (hided)
        {
            timeCounter += Time.deltaTime;
            if(timeCounter >= hideDuration)
            {
                Skill();
            }
        }
    }

    public void Skill()
    {
        if (!hided)
        {
            meshNormal.SetActive(false);
            meshDisfrazada.SetActive(true);

            if (gameManager.onExtra)
            {
                hideDuration += gameManager.extraTime;
            }
            else hideDuration = iniHideDuration;

            hided = true;
        }
        else
        {
            meshNormal.SetActive(true);
            meshDisfrazada.SetActive(false);

            timeCounter = 0;

            hided = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            //Insertar aqui funcion de stun
            other.transform.gameObject.GetComponent<PlayerController>().Stunn();
            meshNormal.SetActive(true);
            meshDisfrazada.SetActive(false);
            //Animacion de cepo cerrandose, al terminar aimación vuelve a abrirse lentamente
            anim.SetTrigger("hit");
        }
    }

}
