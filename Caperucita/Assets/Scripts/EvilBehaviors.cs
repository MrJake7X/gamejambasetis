﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EvilBehaviors : MonoBehaviour
{
    private static GameManager gameManager;

    // trees
    private static bool evilTree = false;
    private static GameObject[] trees;
    private static float INIT_EVIL_TREE_DURATION = 3;
    private static float evilTreeDuration;
    private float evilTreeCounter = 0;

    // flowers
    private static bool evilFlower = false;
    private static GameObject[] flowers;
    private static float INIT_EVIL_FLOWER_DURATION = 3;
    private static float evilFlowerDuration;
    private float evilFlowerCounter = 0;

    // animals
    private static bool evilAnimal = false;
    private static GameObject[] rabbits;
    private static GameObject[] boars;
    private static float INIT_EVIL_ANIMAL_DURATION = 3;
    private static float evilAnimalDuration;
    private float evilAnimalCounter = 0;


    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        trees = GameObject.FindGameObjectsWithTag("Tree");
        flowers = GameObject.FindGameObjectsWithTag("Flor");
        rabbits = GameObject.FindGameObjectsWithTag("Conejo");
        boars = GameObject.FindGameObjectsWithTag("Jabali");
    }

    // Update is called once per frame
    void Update()
    {
        if (evilTree)
        {
            evilTreeCounter += Time.deltaTime;
            if (evilTreeCounter >= evilTreeDuration)
            {
                foreach (GameObject tree in trees) tree.GetComponent<Tree>().SetEvil(false);
                evilTree = false;
            }
        }

        if (evilFlower)
        {
            evilFlowerCounter += Time.deltaTime;
            if (evilFlowerCounter >= evilFlowerDuration)
            {
                foreach (GameObject flower in flowers) flower.GetComponent<Flor>().SetEvil(false);
                evilFlower = false;
            }
        }

        if (evilAnimal)
        {
            evilAnimalCounter += Time.deltaTime;
            if (evilAnimalCounter >= evilAnimalDuration)
            {
                foreach (GameObject rabbit in rabbits) rabbit.GetComponent<Conejito>().SetEvil(false);
                foreach (GameObject boar in boars) boar.GetComponent<Jabali>().SetEvil(false);
                evilAnimal = false;
            }
        }
    }

    public void SetEvilTrees()
    {
        evilTree = true;
        foreach (GameObject tree in trees) tree.GetComponent<Tree>().SetEvil(true);
        if (gameManager.onExtra) evilTreeDuration += gameManager.extraTime;
        else evilTreeDuration = INIT_EVIL_TREE_DURATION;
    }

    public void SetEvilFlowers()
    {
        evilFlower = true;
        foreach (GameObject flower in flowers) flower.GetComponent<Flor>().SetEvil(true);
        if (gameManager.onExtra) evilFlowerDuration += gameManager.extraTime;
        else evilFlowerDuration = INIT_EVIL_FLOWER_DURATION;
    }

    public void SetEvilAnimals()
    {
        evilAnimal = true;
        foreach (GameObject rabbit in rabbits) rabbit.GetComponent<Conejito>().SetEvil(true);
        foreach (GameObject boar in boars) boar.GetComponent<Jabali>().SetEvil(true);
        if (gameManager.onExtra) evilAnimalDuration += gameManager.extraTime;
        else evilAnimalDuration = INIT_EVIL_ANIMAL_DURATION;
    }
}
