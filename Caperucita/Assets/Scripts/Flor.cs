﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Flor : Tree {

    public float moveSpeed = 1;
    // falten molts floats de cuan tarde en fe les coses
    public float love = -1; // locura k cura negativa +
    bool canUse = true;
    int ME = 0;

    //NAV MESH
    public Transform playerPos;
    public Vector3 goal;
    NavMeshAgent agent;

    public void Use() { if (!isEvil && canUse) { cordura.SetCurrentInanity(love); canUse = false; } }
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        ME = 0;
        playerPos = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        base.Start();
    }

    // Update is called once per frame
    void Update()
    {
        //ANIMACION, NAV MESH , ME
        switch (ME)
        {
            case 0: break; // quietas
            case 1: break; // se levantan
            case 2: agent.SetDestination(playerPos.position); break; // te persiguen
            case 3: break; // se arraigan
        }
    }
}
