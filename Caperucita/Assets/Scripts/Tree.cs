﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tree : MonoBehaviour {

    public Insanity cordura;
    public InsaneArea insaneArea;
    
    protected bool isEvil = false;

    public virtual void Start(){
        cordura = GameObject.FindGameObjectWithTag("GameManager").GetComponent<Insanity>();
    }


    public virtual void SetEvil(bool flag)
    {
        isEvil = flag;

        if (isEvil)
        { insaneArea.Active(); }
        else { insaneArea.Desactive(); }
    }
    
    void Update() {

        if (cordura.GetCurrentInanity()==0)
        {

        }
	}



}
