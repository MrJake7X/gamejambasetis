﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    private GameObject[] cepos;

    public PlayerController player;

    [Header("Extra Duration Cards")]
    public float iniExtraTime;
    public float extraTime;
    public float extraDuration;
    private float extraCounter;
    public bool onExtra;

    void Start()
    {
        cepos = GameObject.FindGameObjectsWithTag("Cepo");

        //player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Space))
        {
            //ExtraTime();
        }

        if(onExtra)
        {
            extraCounter += Time.deltaTime;
            if(extraCounter >= extraDuration)
            {
                extraTime = 0;
                extraCounter = 0;
                onExtra = false;
            }
        }
    }

    public void ExtraTime()
    {
        extraTime += iniExtraTime;
        onExtra = true;
    }

    public void HideTraps()
    {
        foreach (GameObject cepo in cepos)
        {
            cepo.GetComponent<Cepo>().Skill();
        }
    }
}
