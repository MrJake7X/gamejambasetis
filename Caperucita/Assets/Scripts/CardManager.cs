﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class CardManager : MonoBehaviour
{
    public Card[] cardList;

    public GameObject cardPrefab;

    public RectTransform slot1;
    public RectTransform slot2;
    public RectTransform slot3;
    public RectTransform slot4;
    public RectTransform slot5;
    public RectTransform spawn;

    private RectTransform card1;
    private RectTransform card2;
    private RectTransform card3;

    public List<GameObject> activeCards;

    public float spawnTime;
    private float timeCounter;

    private int MAX_NUM_CARDS = 3;
    private int NUM_CARDS = 8;

    void Start()
    {
        SpawnCard();
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.R))
        {
            SpawnCard();
            UpdateSlot();
        }
        // NEED IMPROVEMENT
        CheckCards();

        timeCounter += Time.deltaTime;
        if(timeCounter >= spawnTime)
        {
            SpawnCard();
            timeCounter = 0;
        }

        if(EventSystem.current.currentSelectedGameObject == null && activeCards.Count > 0)
        {
            activeCards[0].transform.GetComponentInChildren<Button>().Select();
        }
    }

    public void UpdateSlot()
    {
        switch (activeCards.Count)
        {
            case 0:
                Debug.Log("No hay cartas");
                break;
            case 1:
                card1 = activeCards[0].transform.GetComponent<RectTransform>();
                card1.DOMove(slot2.position, 1).OnComplete(()=> {
                    activeCards[0].transform.GetComponentInChildren<Button>().Select();
                });
                break;
            case 2:
                card1 = activeCards[0].transform.GetComponent<RectTransform>();
                card1.DOMove(slot4.position, 1);

                card2 = activeCards[1].transform.GetComponent<RectTransform>();
                card2.DOMove(slot5.position, 1);
                break;
            case 3:
                card1 = activeCards[0].transform.GetComponent<RectTransform>();
                card1.DOMove(slot3.position, 1);

                card2 = activeCards[1].transform.GetComponent<RectTransform>();
                card2.DOMove(slot2.position, 1);

                card3 = activeCards[2].transform.GetComponent<RectTransform>();
                card3.DOMove(slot1.position, 1);
                break;
            default:

                break;
        }

    }

    public void CheckCards()
    {
        foreach (GameObject card in activeCards)
        {
            if(card.gameObject.GetComponent<CardDisplay>().used)
            {
                int index = activeCards.IndexOf(card);
                Debug.Log("Count: " + activeCards.Count);
                Debug.Log("Index: " + index);
                activeCards.Remove(card);
                Destroy(card);
                
                if (activeCards.Count > 0)
                {
                    if (index <= 0)
                    {
                        activeCards[0].GetComponentInChildren<Button>().Select();
                    }
                    else
                    {
                        activeCards[index - 1].GetComponentInChildren<Button>().Select();
                    }
                }
                break;
            }
        }
        UpdateSlot();
    }

    public void SpawnCard()
    {
        if (activeCards.Count >= MAX_NUM_CARDS)
        {
            Debug.Log("No more cards, thanks");
            return;
        }
        int random = Random.Range(0, NUM_CARDS);
        while (IsCardThere(random)) random = Random.Range(0, NUM_CARDS);

        GameObject card = Instantiate(cardPrefab, spawn);
        activeCards.Add(card);
        card.GetComponent<CardDisplay>().card = cardList[random];

        card.name = card.GetComponent<CardDisplay>().card.name;
    }

    private bool IsCardThere(int rand)
    {
        /*foreach (GameObject card in activeCards)
        {
            if (card.GetComponent<CardDisplay>().card.id == rand) return true;
        }*/
        return false;
    }
}




    