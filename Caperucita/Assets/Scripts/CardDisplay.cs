﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class CardDisplay : MonoBehaviour
{
    private AudioManager audio;
    private GameManager gameManager;
    private PlayerController player;
    private EvilBehaviors evil;

    public Card card;

    public Image image;

    public RectTransform smoke;
    public Image smokeImg;

    public Button bttn;

    public bool used;

    void Start()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        evil = GameObject.FindGameObjectWithTag("GameManager").GetComponent<EvilBehaviors>();
        audio = GameObject.Find("Audios").GetComponent<AudioManager>();

        image.sprite = card.image;

        smoke.DORotate(new Vector3(0, 0, 359), 10, RotateMode.LocalAxisAdd).SetLoops(-1);
    }

    public void Deselect()
    {
        smokeImg.DOFade(0.2f, 1);
    }

    public void Select()
    {
        smokeImg.DOFade(1f, 1);
    }

    public void Use()
    {
        audio.cardActivate.Play();

        switch (card.name)
        {
            case "Animals":
                evil.SetEvilAnimals();
                audio.animales.Play();
                break;
            case "Confusion":
                player.Confusion();
                audio.alreves.Play();
                break;
            case "Darkness":
                audio.oscuridad.Play();
                break;
            case "Faster":
                player.Fast();
                audio.correr.Play();
                break;
            case "Flowers":
                evil.SetEvilFlowers();
                audio.flores.Play();
                break;
            case "Slow":
                player.Slow();
                audio.despacio.Play();
                break;
            case "Traps":
                gameManager.HideTraps();
                audio.trampas.Play();
                break;
            case "Trees":
                evil.SetEvilTrees();
                audio.arboles.Play();
                break;
            default:

                break;
        }
        used = true;
    }

}
