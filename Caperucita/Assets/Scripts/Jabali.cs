﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Jabali : Tree
{
    //NAV MESH
    public Vector3 goal;
    NavMeshAgent agent;
    PlayerController player;
    public float distanciaDeImpacto = 10;
    Vector3 startForward;
    bool isOut = false;


         void Start()
        {
            player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
            agent = GetComponent<NavMeshAgent>();

            Vector3 pos = player.transform.position + player.transform.forward * distanciaDeImpacto;
            goal = pos;
            agent.destination = goal;
        }

    void Update()
    {
        if (agent.pathStatus == NavMeshPathStatus.PathComplete && agent.remainingDistance <= 1 && !isOut)
        {
            startForward = transform.forward;
            agent.destination = agent.destination + startForward;
            isOut = true;
        }
        else if(isOut){ agent.destination = agent.destination + startForward; }

        // disinstanciar fuera de camara>
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            //Insertar aqui funcion de stun
            other.transform.gameObject.GetComponent<PlayerController>().Stunn();
        }
    }
}
