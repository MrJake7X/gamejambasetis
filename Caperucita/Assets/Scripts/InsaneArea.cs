﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InsaneArea : MonoBehaviour {

    public Insanity cordura;
    BoxCollider colider;
    public Vector2 areaSize;
    public float dmg = 1; //locura k suma
    public float timeEfect = 1; //locura x tiempo

    void Awake () {
        colider = GetComponent<BoxCollider>();
        colider.size = new Vector3(areaSize.x,colider.size.y,areaSize.y);
        //Desactive();
        Active();
    }

    void Start(){
        cordura = GameObject.FindGameObjectWithTag("GameManager").GetComponent<Insanity>();
    }

    public void Desactive() { StopCoroutine(UpdateArea()); colider.enabled = false; }
    public void Active() { colider.enabled = true; }

    IEnumerator UpdateArea()
    {
        while (true)
        {
            cordura.SetCurrentInanity(dmg); // prime fa mal despres espere 
            yield return new WaitForSeconds(timeEfect);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        { StartCoroutine(UpdateArea()); }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        { StopCoroutine(UpdateArea()); }
    }

}
