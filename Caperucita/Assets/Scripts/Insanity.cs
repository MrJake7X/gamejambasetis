﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Insanity : MonoBehaviour {

    private AudioManager audio;

    public float maxInsanity = 100;
    public float currentInsanity = 0;
    public float timeLocuraPasiva = 0.5f;
    public float addLocuraPasiva = 0.5f;

    public float GetCurrentInanity() { return currentInsanity; }
    public void SetCurrentInanity(float i)
    {
        if (i > 0) //sumar
        {
            if (currentInsanity + i <= maxInsanity)
            { currentInsanity += i; } else { currentInsanity = maxInsanity; }
        }
        else //restar locura
        {
            if (currentInsanity + i > 0)
            { currentInsanity += i; } else { currentInsanity = 0; }
        }
    }

    IEnumerator LocuraPasiva()
    {
        while (true)
        {
            yield return new WaitForSeconds(timeLocuraPasiva);
            if (currentInsanity + addLocuraPasiva <= maxInsanity)
            { currentInsanity += addLocuraPasiva; }
            else { currentInsanity = maxInsanity; }
        }
    }
    IEnumerator LocuraUpdate()
    {
        while (true)
        {
            if (currentInsanity > 50)
            {
                //ENTORNO LUCES Y DEMAS
            }
            if (currentInsanity > 75)
            {                
              //pCordura1;
              //pCordura2;
              //pCordura3;
              //ambientCorduraBaja;
              //ambientCorduraAlta;
            }

            yield return new WaitForSeconds(1);
        }
    }
    void  Start()
    {
        audio = GameObject.Find("Audios").GetComponent<AudioManager>();
        Init();
    }
    void Init()
    {
        StartCoroutine(LocuraPasiva());
        StartCoroutine(LocuraUpdate());
    }
}
