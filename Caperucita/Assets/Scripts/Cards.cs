﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Cards : MonoBehaviour
{
    private int NUM_CARDS = 11;
    private int MAX_NUM_CARDS = 3;
    private float TIME_BETWEEN_CARDS = 30;

    private float timeLeft = 0;
    private System.Random rand = new System.Random();
    private List<int> listScreen;

    private CardInfo[] listCards = new CardInfo[] {
        new CardInfo(0, "despacio"),
        new CardInfo(1, "confusion"),
        new CardInfo(2, "arboles"),
        new CardInfo(3, "flores"),
        new CardInfo(4, "oscuridad"),
        new CardInfo(5, "animales"),
        new CardInfo(6, "trampas"),
        new CardInfo(7, "superpoderes")/*,
        new CardInfo(8, "name08"),
        new CardInfo(9, "name09"),
        new CardInfo(10, "name10"),*/
        };

    // Use this for initialization
    void Start()
    {
        timeLeft = TIME_BETWEEN_CARDS;
    }

    // Update is called once per frame
    void Update()
    {
        timeLeft -= Time.deltaTime;
        if (timeLeft <= 0)
        {
            ShowCard();
            timeLeft = TIME_BETWEEN_CARDS;
        }
    }

    private void ShowCard()
    {
        if (listScreen.Count >= MAX_NUM_CARDS)
        {
            Debug.Log("S'ha de fer alguna animació com si anés a entrar la carta (?)");
            return;
        }
        // buscar carta
        HashSet<int> allCards = new HashSet<int>();
        for (int i = 0; i < listCards.Length; ++i) allCards.Add(i);
        allCards.ExceptWith(listScreen);
        int card = allCards.ToList()[rand.Next(allCards.Count)];
        listScreen.Add(card);
    }

    public void ClickCard()
    {
        int id = -1; // L'id l'he de treure d'algun lloc
        // TODO: treure carta

        switch (id)
        {
            case 0: // más despacio
                GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().Slow();
                break; 
            case 1: // controles invertidos
                GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().Confusion();
                break; 
            case 2: // árboles te miran
                //EvilBehaviors.SetEvilTrees();
                break;
            case 3: // flores te persiguen
                //EvilBehaviors.SetEvilFlowers();
                break;
            case 4: // oscuridad
                break;
            case 5: // animales poseidos
                //EvilBehaviors.SetEvilAnimals();
                break;
            case 6: // no ves trampas
                GameObject.FindGameObjectWithTag("Cepo").GetComponent<Cepo>().Skill();
                break;
            case 7: // superpoderes
                GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().Fast();
                break;       
        }
    }
}

public class CardInfo : MonoBehaviour {
    public int id;
    public new string name;

    public CardInfo(int id, string name) {
        this.id = id;
        this.name = name;
    }
}

