﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CreateMap : MonoBehaviour {

    public Transform map;
    public GameObject playerPrefab;
    public GameObject cottagePrefab;
    public GameObject treePrefab;
    public GameObject tree2Prefab;
    public GameObject flowerPrefab;
    //public GameObject plantPrefab;
    public GameObject trapPrefab;
    public GameObject rabbitPrefab;
    public GameObject boarPrefab;
    public GameObject bushPrefab;
    public GameObject bush1Prefab;
    public GameObject rockPrefab;
    public GameObject rock2Prefab;
    public GameObject mushroom1Prefab;
    public GameObject mushroom2Prefab;

    private static float SIZE_MAP_X = 100;
    private static float SIZE_MAP_Z = 100;
    private float VISUAL_MAP = 20;
    private float DELTA_POS_PLAYER = 5;

    private float DIST_COTTAGE = 50;
    private float TREE_REAL_SIZE = 1.5f;

    private Info PLAYER = new Info("Player", 1, 1);
    private Info COTTAGE = new Info("Cottage", 3, 0);
    private Info TREE = new Info("Tree", 2, 0);
    private Info FLOWER = new Info("Flower", 1, 0);
    private Info PLANT = new Info("Plant", 1, 0);
    private Info TRAP = new Info("Trap", 1, 0.5676354f);
    private Info RABBIT = new Info("Rabbit", 1, 0);
    private Info BOAR = new Info("Boar", 1, 0);
    private Info BUSH = new Info("Bush", 1, 0);
    private Info ROCK = new Info("Rock", 1, 0);
    private Info MUSHROOM = new Info("Mushroom", 1, 0);


    private int NUM_TREES = 400;
    private int NUM_TREES2 = 400;
    private int NUM_FLOWERS = 100;
    //private int NUM_PLANTS = 1;
    private int NUM_TRAPS = 100;
    private int NUM_RABBITS = 1;
    private int NUM_BOARS = 1;
    private int NUM_BUSH = 100;
    private int NUM_BUSH1 = 100;
    private int NUM_ROCKS = 1;
    private int NUM_ROCKS2 = 1;
    private int NUM_MUSHROOMS = 100;
    private int NUM_MUSHROOMS2 = 100;


    private static System.Random rand = new System.Random();
    private List<InfoToDraw> filled = new List<InfoToDraw>();
    private float lim1x, lim2x, lim1z, lim2z;
    private Transform player;

    private Vector3 initPos;

    // Use this for initialization
    void Start () {
        if (TREE_REAL_SIZE > TREE.radius) TREE_REAL_SIZE = TREE.radius;
        lim1x = lim1z = -DELTA_POS_PLAYER;
        lim2x = lim2z = DELTA_POS_PLAYER;

        // add player (centered)
        /*DrawObject(playerPrefab, new Vector3(0, PLAYER.y, 0), PLAYER);*/
        player = GameObject.FindGameObjectWithTag("Player").transform;
        filled.Add(new InfoToDraw(player.localPosition, PLAYER.radius));
        initPos = player.localPosition;
        // add cottage (at distance Constants.DIST_COTTAGE from the player)
        float angle = NextFloat(0, (float) Math.PI * 2);
        DrawObject(cottagePrefab, new Vector3((float) Math.Sin(angle), COTTAGE.y, (float) Math.Cos(angle)) * DIST_COTTAGE, COTTAGE);
        // add boundary
        DrawBoundary();

        // add trees, flowers, etc.
        for (int i = 0; i < NUM_TREES; ++i) DrawObject(treePrefab, RandPoint(TREE.y), TREE);
        for (int i = 0; i < NUM_TREES2; ++i) DrawObject(tree2Prefab, RandPoint(TREE.y), TREE);
        for (int i = 0; i < NUM_FLOWERS; ++i) DrawObject(flowerPrefab, RandPoint(FLOWER.y), FLOWER);
        //for (int i = 0; i < NUM_PLANTS; ++i) DrawObject(plantPrefab, RandPoint(PLANT.y), PLANT);
        for (int i = 0; i < NUM_TRAPS; ++i) DrawObject(trapPrefab, RandPoint(TRAP.y), TRAP);
        for (int i = 0; i < NUM_RABBITS; ++i) DrawObject(rabbitPrefab, RandPoint(RABBIT.y), RABBIT);
        for (int i = 0; i < NUM_BOARS; ++i) DrawObject(boarPrefab, RandPoint(BOAR.y), BOAR);
        for (int i = 0; i < NUM_BUSH; ++i) DrawObject(bushPrefab, RandPoint(BUSH.y), BUSH);
        for (int i = 0; i < NUM_BUSH1; ++i) DrawObject(bush1Prefab, RandPoint(BUSH.y), BUSH);
        for (int i = 0; i < NUM_ROCKS; ++i) DrawObject(rockPrefab, RandPoint(ROCK.y), ROCK);
        for (int i = 0; i < NUM_ROCKS2; ++i) DrawObject(rockPrefab, RandPoint(ROCK.y), ROCK);
        for (int i = 0; i < NUM_MUSHROOMS; ++i) DrawObject(mushroom1Prefab, RandPoint(MUSHROOM.y), MUSHROOM);
        for (int i = 0; i < NUM_MUSHROOMS2; ++i) DrawObject(mushroom2Prefab, RandPoint(MUSHROOM.y), MUSHROOM);

        // TODO: add every missing element
    }

    void Update () {
        //if (PlayerGoingOutside()) Recenter();
        Vector3 pos = player.localPosition;
        if (player != null)
        {
            if (pos.x > SIZE_MAP_X) pos.x = SIZE_MAP_X;
            if (pos.x < -SIZE_MAP_X) pos.x = -SIZE_MAP_X;
            if (pos.z > SIZE_MAP_Z) pos.z = SIZE_MAP_Z;
            if (pos.z < -SIZE_MAP_Z) pos.z = -SIZE_MAP_Z;
            player.localPosition = pos;
        }
        if (Vector3.Distance(player.localPosition, initPos) > DELTA_POS_PLAYER)
        {
            initPos = player.localPosition;
            UpdateObjects();
        }
    }

    private bool DrawObject(GameObject obj, Vector3 pos, Info info, bool checkSpace = true)
    {
        if (obj == null)
        {
            Debug.Log(">>> WARNING: Object " + info.name + " couldn't be found");
            return false;
        }
        /*if (map == null && !info.name.Equals("player"))
        {
            Debug.Log(">>> WARNING: Map couldn't be found");
            return false;
        }*/
        if (checkSpace) foreach (InfoToDraw elem in filled)
        {
            if (Vector3.Distance(pos, elem.pos) < info.radius + elem.rad) return false;
        }      
        GameObject newObj = Instantiate(obj, map);
        if (obj.tag.Equals("Player")) player = newObj.transform;
        newObj.transform.localPosition = pos;
        // newObj.transform.Rotate(0, UnityEngine.Random.Range(0f, 2f * Math.PI), 0);
        newObj.name = info.name;
        filled.Add(new InfoToDraw(pos, info.radius));
        if (Vector3.Distance(player.localPosition, newObj.transform.localPosition) > VISUAL_MAP) newObj.SetActive(false);
        return true;
    }

    private void DrawBoundary()
    {
        Vector3 pos = new Vector3(-SIZE_MAP_X, 0, -SIZE_MAP_Z);
        while (pos.x < SIZE_MAP_X)
        {
            DrawObject(treePrefab, pos, TREE, false);
            pos += new Vector3(TREE_REAL_SIZE, 0, 0);
        }
        while (pos.z < SIZE_MAP_Z)
        {
            DrawObject(treePrefab, pos, TREE, false);
            pos += new Vector3(0, 0, TREE_REAL_SIZE);
        }
        while (pos.x > -SIZE_MAP_X)
        {
            DrawObject(treePrefab, pos, TREE, false);
            pos -= new Vector3(TREE_REAL_SIZE, 0, 0);
        }
        while (pos.z > -SIZE_MAP_Z)
        {
            DrawObject(treePrefab, pos, TREE, false);
            pos -= new Vector3(0, 0, TREE_REAL_SIZE);
        }
    }

    private bool PlayerGoingOutside()
    {
        if (player == null)
        {
            Debug.Log(">>> WARNING: Player not found");
            return false;
        }
        Vector3 pos = player.localPosition;
        if (Math.Abs(pos.x - (lim1x + lim2x)/2) > DELTA_POS_PLAYER) return true;
        if (Math.Abs(pos.z - (lim1z + lim2z)/2) > DELTA_POS_PLAYER) return true;
        return false;
    }

    private void UpdateObjects()
    {
        foreach (Transform child in map) {
          
            try
            {
                if (Vector3.Distance(child.localPosition, player.localPosition) < VISUAL_MAP) child.gameObject.SetActive(true);
                else child.gameObject.SetActive(false);
            }
            catch (Exception e)
            {
                Debug.Log(e.Message);
            }
        }
    }

    /*private void Recenter()
    {
        lim1x = player.localPosition.x - SIZE_MAP_X;
        lim2x = player.localPosition.x + SIZE_MAP_X;
        lim1z = player.localPosition.z - SIZE_MAP_Z;
        lim2z = player.localPosition.z + SIZE_MAP_Z;

        foreach(Transform child in map)
        {
            Vector3 pos = child.localPosition;
            if (pos.x < lim1x) pos += new Vector3(2 * SIZE_MAP_X, 0, 0);
            else if (pos.x > lim2x) pos -= new Vector3(2 * SIZE_MAP_X, 0, 0);
            if (pos.z < lim1z) pos += new Vector3(0, 0, 2 * SIZE_MAP_Z);
            else if (pos.z > lim2z) pos -= new Vector3(0, 0, 2 * SIZE_MAP_Z);
            child.localPosition = pos;
        }
    }*/

    private static Vector3 RandPoint(float y = 0)
    {
        Vector3 pos = Vector3.zero;
        pos.x = NextFloat(-SIZE_MAP_X, SIZE_MAP_X);
        pos.y = y;
        pos.z = NextFloat(-SIZE_MAP_Z, SIZE_MAP_Z);
        return pos;
    }

    private static float NextFloat(float lim1, float lim2)
    {
        return (float)rand.NextDouble() * (lim2 - lim1) + lim1;
    }
}

public class Info
{
    public string name;
    public float radius;
    public float y;

    public Info(string name, float radius, float y)
    {
        this.name = name;
        this.radius = radius;
        this.y = y;
    }
}

public class InfoToDraw : MonoBehaviour
{
    public Vector3 pos;
    public float rad;

    public InfoToDraw(Vector3 pos, float rad)
    {
        this.pos = pos;
        this.rad = rad;
    }

    public InfoToDraw(float x, float z, float rad)
    {
        new InfoToDraw(new Vector3(x, 0, z), rad);
    }
}
