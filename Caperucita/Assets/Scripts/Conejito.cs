﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public class Conejito : Tree
{

    public float moveSpeed = 1;

    public float timeToPlaying = 1.5f;
    float countTimePlaying = 0;
    bool isPlaying = false;
   
    public float love = -1; // locura k cura negativa --
    public int ME = 0;
    bool canUse = true;

    public GameObject conejoNormal;
    public Animator anim1;
    public GameObject conejoDistorsionado;
    public Animator anim2;

    //NAV MESH
    public float radiZonaFun = 5; //zona de juegos
    public float radiZonaOut = 20; //zona de juegos
    public Terrain map;
    public Vector3 goal;
    NavMeshAgent agent;
    private Vector3 centroZona;

    public void Use() { if (canUse && !isEvil) { cordura.SetCurrentInanity(love); canUse = false; ME = 2; } }
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        ME = 0;
        conejoDistorsionado.SetActive(false);

    } 
    void Update()
    {
        //ANIMACION, NAV MESH , ME
        switch (ME)
        {
            case 0: ME++; canUse = true; centroZona = transform.position;  isPlaying = false; break; 
            case 1:// correteando por ahi
                if (agent.pathStatus == NavMeshPathStatus.PathComplete && agent.remainingDistance == 0 && !isPlaying) { CambiarPosicionInZone(); }
                break; 
            case 2:// deja de jugar en su zona para huir
                CambiarZone();
                ME = 3;
                break;
            case 3: // de camino a una nueva zona
                if (agent.pathStatus == NavMeshPathStatus.PathComplete && agent.remainingDistance == 0) { ME = 0; } // si ha llegado
                break;
        }

        anim1.SetBool("Run", !agent.isStopped);

        anim2.SetBool("Run", !agent.isStopped);
    }

    public override void SetEvil(bool flag)
    {
        if(flag)
        {
            conejoNormal.SetActive(false);
            conejoDistorsionado.SetActive(true);
        }
        else
        {
            conejoNormal.SetActive(true);
            conejoDistorsionado.SetActive(false);
        }
        base.SetEvil(flag);
    }

    void CambiarPosicionInZone()
    {
        StartCoroutine(CountTimePlaying());
        float x, y = transform.position.y, z;
        x = Random.Range(centroZona.x - radiZonaFun / 2, centroZona.x + radiZonaFun / 2);
        z = Random.Range(centroZona.z - radiZonaFun / 2, centroZona.z + radiZonaFun / 2);
        while (Vector2.Distance(centroZona, new Vector3(x, y, z)) > radiZonaFun && agent.CalculatePath(new Vector3(x, y, z),agent.path))
        {
            x = Random.Range(centroZona.x - radiZonaFun / 2, centroZona.x + radiZonaFun / 2);
            z = Random.Range(centroZona.z - radiZonaFun / 2, centroZona.z + radiZonaFun / 2);
        }
        goal = new Vector3(x, y, z);
        agent.destination = goal;
    }

    void CambiarZone()
    {
        float x, y = transform.position.y, z;
        x = Random.Range(0, map.terrainData.size.x);
        z = Random.Range(0, map.terrainData.size.z);
        while (Vector2.Distance(centroZona, new Vector3(x, y, z)) < radiZonaOut && agent.CalculatePath(new Vector3(x, y, z), agent.path))
        {
            x = Random.Range(0, map.terrainData.size.x);
            z = Random.Range(0, map.terrainData.size.z);
        }
        goal = new Vector3(x, y, z);
        agent.destination = goal;
    }
    IEnumerator CountTimePlaying()
    {
        isPlaying = true;
        while (countTimePlaying < timeToPlaying)
        { yield return new WaitForSeconds(1); countTimePlaying += 1; }
        countTimePlaying = 0; isPlaying = false;
    }

}
