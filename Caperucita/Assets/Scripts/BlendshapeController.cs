﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlendshapeController : MonoBehaviour
{

    public int blendShapeCount;

    SkinnedMeshRenderer skinnedMeshRenderer;

    Mesh skinnedMesh;

    public float blendOne;
    public float blendTwo;
    public float blendSpeed = 1f;
    public bool blendOneFinished = false;

    private void Awake()
    {
        skinnedMeshRenderer = GetComponent<SkinnedMeshRenderer>();
        skinnedMesh = GetComponent<SkinnedMeshRenderer>().sharedMesh;
    }

    void Start()
    {
        blendShapeCount = skinnedMesh.blendShapeCount;
    }

    void Update()
    {
        if(blendShapeCount >= 1)
        {
            if(blendOne < 100)
            {
                skinnedMeshRenderer.SetBlendShapeWeight(0, blendOne);
                blendOne += blendSpeed;
            }
            else
            {
                blendOneFinished = true;
            }

            if (blendTwo < 100 && blendOneFinished)
            {
                skinnedMeshRenderer.SetBlendShapeWeight(1, blendTwo);
                blendTwo += blendSpeed;
            }
        }
    }
}
